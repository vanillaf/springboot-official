# README

<div align="center">
<img src="src/main/resources/static/img/butterflies.png" height=128  alt="springboot">
</div>

## HELP

## INFO

- more details on localhost:9003/help or http://localhost:9003/help (after run current springboot project)

## TODO

- [x] check upload image, maybe need setup redis client more steps
- [x] show image ?
- [x] img to static(optional)
- [ ] list image bug, but another is work
- [ ] check functions
- [ ] create offcnpe_clean to clean unused images

## REPO

- address: https://gitlab.com/vanillaf/springboot-official
- download zip: https://gitlab.com/vanillaf/springboot-official/-/archive/main/springboot-official-main.zip

## NOTE

- use mariadb not mysql
- use 8080 port not 9003
- etc

## PROGRESS

- [x] task 06

## MISC

- https://blog.csdn.net/THcoding_Cat/article/details/92004141
- https://www.flaticon.com/stickers-pack/pets-92
- https://www.codeleading.com/article/78313244749/#:~:text=1.%E5%9C%A8%E9%80%89%E6%8B%A9%E7%85%A7%E7%89%87%E6%97%B6%E4%B8%8A%E4%BC%A0%E5%88%B0%E4%BA%86%E5%9B%BE%E7%89%87%E5%82%A8%E5%AD%98%E6%88%91%E4%BB%AC%E5%8F%AF%E4%BB%A5%E5%B0%86%E4%BF%A1%E6%81%AF%E4%BF%9D%E5%AD%98%E4%B8%80%E4%BB%BD%E5%88%B0redis%EF%BC%8C%E8%BF%99%E6%98%AF%E4%B8%8A%E4%BC%A0%E6%88%90%E5%8A%9F%E7%9A%84%E7%85%A7%E7%89%87%E7%9A%84%E9%9B%86%E5%90%88%EF%BC%9B%E7%84%B6%E5%90%8E%E5%B0%86%E7%85%A7%E7%89%87%E7%9A%84%E5%AD%98%E5%82%A8%E5%9C%B0%E5%9D%80%E8%BF%94%E5%9B%9E%EF%BC%8C%E5%9C%A8%E5%89%8D%E7%AB%AF%E9%A1%B5%E9%9D%A2%E8%BF%9B%E9%A2%84%E8%A7%88%E5%92%8C%E5%9B%9E%E6%98%BE%EF%BC%9B,2.%E5%9C%A8%E7%82%B9%E5%87%BB%E7%A1%AE%E8%AE%A4%E5%90%8E%EF%BC%8C%E5%B0%86%E6%95%B0%E6%8D%AE%E4%BF%9D%E5%AD%98%E5%9C%A8%E6%95%B0%E6%8D%AE%E5%BA%93%EF%BC%8C%E5%90%8C%E6%A0%B7%E7%A1%AE%E8%AE%A4%E7%9A%84%E7%85%A7%E7%89%87%E4%BF%A1%E6%81%AF%E4%BF%9D%E5%AD%98%E4%B8%80%E4%BB%BD%E5%88%B0redis%E7%9A%84%E9%9B%86%E5%90%88%E4%B8%AD%EF%BC%8C%E8%BF%99%E4%B8%AA%E9%9B%86%E5%90%88%E4%B8%BA%E7%A1%AE%E8%AE%A4%E5%90%8E%E7%9A%84%E7%85%A7%E7%89%87%E4%BF%A1%E6%81%AF%E9%9B%86%E5%90%88%EF%BC%9B%203.%E5%9C%A8%E4%B8%80%E5%AE%9A%E5%91%A8%E6%9C%9F%E8%B0%83%E7%94%A8%E5%A4%84%E7%90%86%E5%9E%83%E5%9C%BE%E5%9B%BE%E7%89%87%E7%9A%84%E6%9C%8D%E5%8A%A1%E8%BF%9B%E8%A1%8C%E4%B8%80%E4%B8%AA%E6%B8%85%E7%90%86%EF%BC%9B%E5%88%A9%E7%94%A8redis%E9%9B%86%E5%90%88%E7%9A%84%E7%89%B9%E6%80%A7%EF%BC%8C%E6%89%BE%E5%88%B0%E8%BF%99%E4%B8%A4%E4%B8%AA%E9%9B%86%E5%90%88%E7%9A%84%E5%B7%AE%E9%9B%86%EF%BC%8C%E4%B9%9F%E5%B0%B1%E6%98%AF%E6%97%A0%E6%95%88%E5%9B%BE%E7%89%87%EF%BC%8C%E8%BF%9B%E4%B8%80%E4%B8%AA%E5%88%A0%E9%99%A4%EF%BC%9B
