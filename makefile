offcnpe-template-file-help := src/main/resources/static/template/offcnpe_help.html
offcnpe-file-help := src/main/resources/static/pages/offcnpe_help.html
content := <script src="../js/confetti.js"></script><script src="../js/confetti-fireworks.js"></script>\n
icon := ../img/asset-favico.ico
port := 9003
version := v0.0.1
jarName := target/offcnpe-$(version).jar

run:
	@echo  "runing on localhost:$(port)"
	@java -jar target/offcnpe-v0.0.1.jar --server.port=$(port)

start:
	@sudo systemctl start redis
	@sudo systemctl start mysql
	@echo " enabled redis and mysql"

build:
	@mvn clean package -D maven.test.skip=true
	@ls -sh $(jarName)

patch:
	@cp $(offcnpe-template-file-help) $(offcnpe-file-help)
	@sed -i -e "s#favicon.ico#$(icon)#" -e "s#</body>#$(content)&#" -e "s#<title>#&OFFCNPE HELP 🚀 by @oeyoews#" $(offcnpe-file-help)
