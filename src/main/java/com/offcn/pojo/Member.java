package com.offcn.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author zs
 * @since 2022-10-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_member")
public class Member extends Model {

  private static final long serialVersionUID = 1L;

  @TableId(value = "id", type = IdType.AUTO)
  private Integer id;

  @TableField("fileNumber")
  private String filenumber;

  private String name;

  private String sex;

  @TableField("idCard")
  private String idcard;

  @TableField("phoneNumber")
  private String phonenumber;

  @TableField("regTime")
  private Date regtime;

  private String password;

  private String email;

  private Date birthday;

  private String remark;


}
