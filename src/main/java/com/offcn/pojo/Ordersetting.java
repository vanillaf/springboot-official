package com.offcn.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author zs
 * @since 2022-10-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_ordersetting")
public class Ordersetting extends Model {

  private static final long serialVersionUID = 1L;

  @TableId(value = "id", type = IdType.AUTO)
  private Integer id;

  /**
   * 约预日期
   */
  @TableField("orderDate")
  private Date orderdate;

  /**
   * 可预约人数
   */
  private Integer number;

  /**
   * 已预约人数
   */
  private Integer reservations;


}
