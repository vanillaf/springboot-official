package com.offcn.controller;


import com.offcn.service.MemberService;
import com.offcn.util.Result;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zs
 * @since 2022-10-11
 */
@RestController
@RequestMapping("/member")
public class MemberController {


  @Resource
  private MemberService memberService;

  @RequestMapping("/getMemberReport")
  public Result getMemberReport() {
    return memberService.getMemberReport();
  }
}

