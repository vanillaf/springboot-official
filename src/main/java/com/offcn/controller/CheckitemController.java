package com.offcn.controller;


import com.offcn.pojo.Checkitem;
import com.offcn.service.CheckItemService;
import com.offcn.util.PageResult;
import com.offcn.util.QueryPageBean;
import com.offcn.util.Result;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zs
 * @since 2022-10-11
 */
@RestController
@RequestMapping("/checkitem")
public class CheckitemController {


    @Resource
    private CheckItemService checkItemService;

    @PreAuthorize("hasAuthority('CHECKITEM_QUERY')")
    @RequestMapping("/getCheckItems")
    public PageResult getCheckItems(@RequestBody  QueryPageBean queryPageBean){

        return checkItemService.getCheckItems(queryPageBean);
    }

    @PreAuthorize("hasAuthority('CHECKITEM_ADD')")
    @RequestMapping("/addCheckItem")
    public Result addCheckItem(@RequestBody Checkitem checkitem){

        return checkItemService.addCheckItem(checkitem);
    }

    @PreAuthorize("hasAuthority('CHECKITEM_EDIT')")
    @RequestMapping("/updateCheckItem")
    public Result updateCheckItem(@RequestBody Checkitem checkitem){
        return checkItemService.updateCheckItem(checkitem);
    }

    @PreAuthorize("hasAuthority('CHECKITEM_DELETE')")
    @RequestMapping("/deleteCheckItemById")
    public Result deleteCheckItemById(int id){
        return checkItemService.deleteCheckItemById(id);
    }

    @RequestMapping("/getAllCheckitems")
    public Result getAllCheckitems(){

        return checkItemService.getAllCheckitems();
    }
}

