package com.offcn.controller;


import com.offcn.pojo.Setmeal;
import com.offcn.service.SetmealService;
import com.offcn.util.PageResult;
import com.offcn.util.QueryPageBean;
import com.offcn.util.Result;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zs
 * @since 2022-10-11
 */
@RestController
@RequestMapping("/setmeal")
public class SetmealController {

  @Resource
  private SetmealService setmealService;
  @Resource
  private RedisTemplate<String, String> redisTemplate;

  @RequestMapping("/getSetmeals")
  public PageResult getSetmeals(@RequestBody QueryPageBean queryPageBean) {

    return setmealService.getSetmeals(queryPageBean);
  }

  @RequestMapping("/upload")
  public Result upload(MultipartFile imgFile) {

    try {
      //确定义上传的目录
      //File saveDirFile = new File("/home/oeyoews/temp/offcnpe-images/upload/images");
      File saveDirFile = new File("/home/oeyoews/workspace/xingyang/upload/images");

      //如果目录不存在就创建
      if (!saveDirFile.exists()) saveDirFile.mkdirs();
      //获取文件的名称
      String oldFileName = imgFile.getOriginalFilename();
      //使用UUID生成文件唯一名称

      String newFileName = UUID.randomUUID().toString() + "&" + oldFileName;
      //String newFileName = UUID.randomUUID() + "&" + oldFileName;
      //定义要保存的文件
      File newFile = new File(saveDirFile, newFileName);
      //上传
      imgFile.transferTo(newFile);

      //缓存图片
      redisTemplate.opsForSet().add("UPLOAD_PIC", newFileName);

      return new Result(true, "上传成功", newFileName);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return new Result(false, "上传失败");
  }

  @RequestMapping("/addSetmeal")
  public Result addSetmeal(@RequestBody Setmeal setmeal, int[] checkgroupIds) {

    Result result = setmealService.addSetmeal(setmeal, checkgroupIds);
    if (result.isFlag()) {
      redisTemplate.opsForSet().add("UPLOAD_PIC_DB", setmeal.getImg());
    }
    return result;
  }

  @RequestMapping("/getAllSetmeal")
  public Result getAllSetmeal() {
    return setmealService.getAllSetmeal();
  }

  @RequestMapping("/findInfoById")
  public Result findInfoById(int id) {

    return setmealService.findInfoById(id);
  }

  @RequestMapping("/getSetmealReport")
  public Result getSetmealReport() {

    return setmealService.getSetmealReport();
  }
}

