package com.offcn.controller;


import com.offcn.pojo.Checkgroup;
import com.offcn.service.CheckGroupService;
import com.offcn.util.PageResult;
import com.offcn.util.QueryPageBean;
import com.offcn.util.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zs
 * @since 2022-10-11
 */
@RestController
@RequestMapping("/checkgroup")
public class CheckgroupController {

  @Resource
  private CheckGroupService checkGroupService;

  @RequestMapping("/getCheckGroups")
  public PageResult getCheckGroups(@RequestBody QueryPageBean queryPageBean) {

    return checkGroupService.getCheckGroups(queryPageBean);

  }

  @RequestMapping("/addCheckGroup")
  public Result addCheckGroup(@RequestBody Checkgroup checkgroup, int[] checkitemIds) {

    return checkGroupService.addCheckGroup(checkgroup, checkitemIds);
  }

  @RequestMapping("/getSelectedCheckItemIdsByCheckGroupId")
  public Result getSelectedCheckItemIdsByCheckGroupId(int id) {
    return checkGroupService.getSelectedCheckItemIdsByCheckGroupId(id);
  }

  @RequestMapping("/updateCheckGroup")
  public Result updateCheckGroup(@RequestBody Checkgroup checkgroup, int[] checkitemIds) {

    return checkGroupService.updateCheckGroup(checkgroup, checkitemIds);
  }

  @RequestMapping("/deleteCheckGroup")
  public Result deleteCheckGroup(int id) {
    return checkGroupService.deleteCheckGroup(id);
  }

  @RequestMapping("/getAllCheckgroups")
  public Result getAllCheckgroups() {
    return checkGroupService.getAllCheckgroups();
  }
}

