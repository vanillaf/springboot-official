package com.offcn.controller;


import com.offcn.pojo.Ordersetting;
import com.offcn.service.OrderSettingService;
import com.offcn.util.POIUtils;
import com.offcn.util.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zs
 * @since 2022-10-11
 */
@RestController
@RequestMapping("/ordersetting")
public class OrdersettingController {

  @Resource
  private OrderSettingService orderSettingService;

  @RequestMapping("/uploadTempleate")
  public Result uploadTempleate(MultipartFile excelFile) {

    try {
      ////{[2021-09-09,100],[2021-09-10,200]}
      List<String[]> strings = POIUtils.readExcel(excelFile);
      orderSettingService.saveBatchOrderSettings(strings);
      return new Result(true, "预约数据导入成功");
    } catch (IOException e) {
      e.printStackTrace();
    }
    return new Result(false, "预约数据导入失败");

  }

  @RequestMapping("/getOrdersettingByDate")
  public Result getOrdersettingByDate(String date) {

    return orderSettingService.getOrdersettingByDate(date);
  }

  @RequestMapping("/editNumberByDate")
  public Result editNumberByDate(@RequestBody Ordersetting ordersetting) {

    return orderSettingService.editNumberByDate(ordersetting);
  }
}

