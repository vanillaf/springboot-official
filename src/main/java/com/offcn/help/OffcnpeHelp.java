package com.offcn.help;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OffcnpeHelp {
  @RequestMapping("/")
  public String offcnpeHelp() {
    String author = "@oeyoews";
    String help = "pages/offcnpe_help.html";
    System.out.println("Offcnpe help, coming ... by " + author);
    return help;
  }
}
