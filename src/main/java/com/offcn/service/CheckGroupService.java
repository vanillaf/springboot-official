package com.offcn.service;

import com.offcn.pojo.Checkgroup;
import com.offcn.util.PageResult;
import com.offcn.util.QueryPageBean;
import com.offcn.util.Result;

public interface CheckGroupService {
  PageResult getCheckGroups(QueryPageBean queryPageBean);

  Result addCheckGroup(Checkgroup checkgroup, int[] checkitemIds);

  Result getSelectedCheckItemIdsByCheckGroupId(int id);

  Result updateCheckGroup(Checkgroup checkgroup, int[] checkitemIds);

  Result deleteCheckGroup(int id);

  Result getAllCheckgroups();
}
