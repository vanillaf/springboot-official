package com.offcn.service;

import com.offcn.pojo.Setmeal;
import com.offcn.util.PageResult;
import com.offcn.util.QueryPageBean;
import com.offcn.util.Result;

public interface SetmealService {
  PageResult getSetmeals(QueryPageBean queryPageBean);

  Result addSetmeal(Setmeal setmeal, int[] checkgroupIds);

  Result getAllSetmeal();

  Result findInfoById(int id);

  Result getSetmealReport();
}
