package com.offcn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.offcn.mapper.OrdersettingMapper;
import com.offcn.pojo.Ordersetting;
import com.offcn.service.OrderSettingService;
import com.offcn.util.Result;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class OrderSettingServiceImpl implements OrderSettingService {

  @Resource
  private OrdersettingMapper ordersettingMapper;

  @Override
  public void saveBatchOrderSettings(List<String[]> strings) {
    if (strings != null && strings.size() > 0) {
      for (String[] string : strings) {
        try {
          SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
          Date orderDate = sdf.parse(string[0]);
          Integer number = Integer.parseInt(string[1]);
          //查询数据库ordersetting表中有没有该日期
          QueryWrapper<Ordersetting> queryWrapper = new QueryWrapper<>();
          queryWrapper.eq("orderDate", orderDate);
          Ordersetting ordersetting = ordersettingMapper.selectOne(queryWrapper);
          if (ordersetting != null) {
            ordersetting.setNumber(number);
            ordersettingMapper.updateById(ordersetting);
          } else {
            ordersetting = new Ordersetting();
            ordersetting.setOrderdate(orderDate);
            ordersetting.setNumber(number);
            ordersetting.setReservations(0);
            ordersettingMapper.insert(ordersetting);
          }
        } catch (ParseException e) {
          e.printStackTrace();
          throw new RuntimeException();
        }
      }
    }
  }

  @Override
  public Result getOrdersettingByDate(String date) {

    List<Map> mapList = new ArrayList<>();
    String startDate = date + "-01";
    String endDate = date + "-31";
    QueryWrapper<Ordersetting> queryWrapper = new QueryWrapper<>();
    queryWrapper.between("orderDate", startDate, endDate);
    List<Ordersetting> ordersettingList = ordersettingMapper.selectList(queryWrapper);
    for (Ordersetting ordersetting : ordersettingList) {
      Map map = new HashMap();
      Calendar instance = Calendar.getInstance();
      instance.setTime(ordersetting.getOrderdate());
      int day = instance.get(Calendar.DAY_OF_MONTH);
      map.put("date", day);
      map.put("number", ordersetting.getNumber());
      map.put("reservations", ordersetting.getReservations());
      mapList.add(map);
    }
    return new Result(true, "", mapList);
  }

  @Override
  public Result editNumberByDate(Ordersetting ordersetting) {
    QueryWrapper<Ordersetting> queryWrapper = new QueryWrapper<>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String OrderDate = sdf.format(ordersetting.getOrderdate());
    queryWrapper.eq("orderDate", OrderDate);
    Ordersetting oldOrdersetting = ordersettingMapper.selectOne(queryWrapper);
    oldOrdersetting.setNumber(ordersetting.getNumber());
    int rows = ordersettingMapper.updateById(oldOrdersetting);
    if (rows >= 1) {
      return new Result(true, "更新成功");
    }
    return new Result(false, "更新失败");
  }
}
