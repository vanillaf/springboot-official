package com.offcn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.offcn.mapper.PermissionMapper;
import com.offcn.mapper.UserMapper;
import com.offcn.pojo.Permission;
import com.offcn.pojo.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Component
public class MyUserDetailsService implements UserDetailsService {

    @Resource
    private UserMapper userMapper;
    @Resource
    private PermissionMapper permissionMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        QueryWrapper<User>  queryWrapper=new QueryWrapper<User>();
        queryWrapper.eq("username",username);
        User user = userMapper.selectOne(queryWrapper);
        if(user==null){
            throw new UsernameNotFoundException("用户名不存在");
        }

        //创建一个集合 放角色和权限
        List<GrantedAuthority> grantedAuthorityList=new ArrayList<>();
        List<Permission> permissionList = permissionMapper.getPermissionsByUserId(user.getId());
        for (Permission permission : permissionList) {
            SimpleGrantedAuthority simpleGrantedAuthority=new SimpleGrantedAuthority(permission.getKeyword());
            grantedAuthorityList.add(simpleGrantedAuthority);
        }

        return new org.springframework.security.core.userdetails.User(username,user.getPassword(),grantedAuthorityList);
    }
}
