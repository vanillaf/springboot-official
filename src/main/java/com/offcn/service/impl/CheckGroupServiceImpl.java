package com.offcn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.offcn.mapper.CheckgroupCheckitemMapper;
import com.offcn.mapper.CheckgroupMapper;
import com.offcn.pojo.Checkgroup;
import com.offcn.pojo.CheckgroupCheckitem;
import com.offcn.pojo.SetmealCheckgroup;
import com.offcn.service.CheckGroupService;
import com.offcn.util.MessageConstant;
import com.offcn.util.PageResult;
import com.offcn.util.QueryPageBean;
import com.offcn.util.Result;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class CheckGroupServiceImpl implements CheckGroupService {

  @Resource
  private CheckgroupMapper checkgroupMapper;
  @Resource
  private CheckgroupCheckitemMapper checkgroupCheckitemMapper;

  @Override
  public PageResult getCheckGroups(QueryPageBean queryPageBean) {

    //1.设置分页
    Page<Checkgroup> page = new Page<>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
    //2.条件
    QueryWrapper<Checkgroup> queryWrapper = new QueryWrapper<>();
    if (queryPageBean.getQueryString() != null && !"".equals(queryPageBean.getQueryString())) {
      queryWrapper.like("code", queryPageBean.getQueryString())
              .or().like("name", queryPageBean.getQueryString())
              .or().like("helpCode", queryPageBean.getQueryString());
    }
    //3.查询
    Page<Checkgroup> checkgroupPage = checkgroupMapper.selectPage(page, queryWrapper);

    return new PageResult(checkgroupPage.getTotal(), checkgroupPage.getRecords());
  }

  @Override
  public Result addCheckGroup(Checkgroup checkgroup, int[] checkitemIds) {
    //先保存检查组
    int rows = checkgroupMapper.insert(checkgroup);

    if (checkitemIds != null && checkitemIds.length > 0) {
      for (int checkitemId : checkitemIds) {
        CheckgroupCheckitem checkgroupCheckitem = new CheckgroupCheckitem();
        checkgroupCheckitem.setCheckgroupId(checkgroup.getId());
        checkgroupCheckitem.setCheckitemId(checkitemId);
        checkgroupCheckitemMapper.insert(checkgroupCheckitem);
      }

    }

    if (rows >= 1) {
      return new Result(true, MessageConstant.ADD_CHECKGROUP_SUCCESS);
    }
    return new Result(false, MessageConstant.ADD_CHECKGROUP_FAIL);
  }

  @Override
  public Result getSelectedCheckItemIdsByCheckGroupId(int id) {
    List<Integer> checkItemIds = new ArrayList<>();
    QueryWrapper<CheckgroupCheckitem> queryWrapper = new QueryWrapper<>();
    queryWrapper.eq("checkgroup_id", id);
    List<CheckgroupCheckitem> checkgroupCheckitemList = checkgroupCheckitemMapper.selectList(queryWrapper);
    for (CheckgroupCheckitem checkgroupCheckitem : checkgroupCheckitemList) {
      checkItemIds.add(checkgroupCheckitem.getCheckitemId());
    }
    return new Result(true, "", checkItemIds);
  }

  @Override
  public Result updateCheckGroup(Checkgroup checkgroup, int[] checkitemIds) {

    //更新检查组的基本信息
    int rows = checkgroupMapper.updateById(checkgroup);
    //删除中间表中该检查组所拥有的检查项
    QueryWrapper<CheckgroupCheckitem> deleteQueryWrapper = new QueryWrapper<>();
    deleteQueryWrapper.eq("checkgroup_id", checkgroup.getId());
    checkgroupCheckitemMapper.delete(deleteQueryWrapper);

    //重新把该检查组所拥有的检查项进行保存
    if (checkitemIds != null && checkitemIds.length > 0) {
      for (int checkitemId : checkitemIds) {
        CheckgroupCheckitem checkgroupCheckitem = new CheckgroupCheckitem();
        checkgroupCheckitem.setCheckgroupId(checkgroup.getId());
        checkgroupCheckitem.setCheckitemId(checkitemId);
        checkgroupCheckitemMapper.insert(checkgroupCheckitem);
      }

    }

    if (rows >= 1) {
      return new Result(true, MessageConstant.EDIT_CHECKGROUP_SUCCESS);
    }
    return new Result(false, MessageConstant.EDIT_CHECKGROUP_FAIL);
  }

  @Override
  public Result deleteCheckGroup(int id) {
    //删除检查组与检查项中间表数据
    QueryWrapper<CheckgroupCheckitem> deleteCheckGroupCheckItem = new QueryWrapper<>();
    deleteCheckGroupCheckItem.eq("checkgroup_id", id);
    checkgroupCheckitemMapper.delete(deleteCheckGroupCheckItem);
    //删除检查组与套餐中间表数据
    QueryWrapper<SetmealCheckgroup> deleteSetmealCheckgroup = new QueryWrapper<>();
    deleteSetmealCheckgroup.eq("checkgroup_id", deleteSetmealCheckgroup);
    //删除检查组
    int rows = checkgroupMapper.deleteById(id);
    if (rows >= 1) {
      return new Result(true, MessageConstant.DELETE_CHECKGROUP_SUCCESS);
    }
    return new Result(false, MessageConstant.DELETE_CHECKGROUP_FAIL);
  }

  @Override
  public Result getAllCheckgroups() {
    List<Checkgroup> checkgroupList = checkgroupMapper.selectList(null);
    return new Result(true, "", checkgroupList);
  }
}
