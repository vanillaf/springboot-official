package com.offcn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.offcn.mapper.MemberMapper;
import com.offcn.pojo.Member;
import com.offcn.service.MemberService;
import com.offcn.util.Result;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class MemberServiceImpl implements MemberService {

  @Resource
  private MemberMapper memberMapper;

  @Override
  public Result getMemberReport() {

    //集合用于存放月份
    List<String> monthList = new ArrayList<>();
    //集合用于存放每个月的会员量
    List<Integer> countList = new ArrayList<>();

    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.MONTH, -12);
    for (int i = 0; i < 12; i++) {
      calendar.add(Calendar.MONTH, 1);
      Date time = calendar.getTime();
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
      monthList.add(sdf.format(time));
    }

    for (String month : monthList) {
      String startDate = month + "-01";
      String endDate = month + "-31";
      QueryWrapper<Member> queryWrapper = new QueryWrapper<>();
      queryWrapper.between("regTime", startDate, endDate);
      Integer count = memberMapper.selectCount(queryWrapper);
      countList.add(count);
    }
    Map map = new HashMap();
    map.put("months", monthList);
    map.put("memberCount", countList);

    return new Result(true, "", map);
  }
}
