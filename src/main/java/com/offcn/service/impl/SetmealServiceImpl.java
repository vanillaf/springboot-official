package com.offcn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.offcn.mapper.*;
import com.offcn.pojo.*;
import com.offcn.service.SetmealService;
import com.offcn.util.MessageConstant;
import com.offcn.util.PageResult;
import com.offcn.util.QueryPageBean;
import com.offcn.util.Result;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SetmealServiceImpl implements SetmealService {

  @Resource
  private SetmealMapper setmealMapper;
  @Resource
  private SetmealCheckgroupMapper setmealCheckgroupMapper;
  @Resource
  private CheckgroupCheckitemMapper checkgroupCheckitemMapper;
  @Resource
  private CheckgroupMapper checkgroupMapper;
  @Resource
  private CheckitemMapper checkitemMapper;
  @Resource
  private OrderMapper orderMapper;

  @Override
  public PageResult getSetmeals(QueryPageBean queryPageBean) {

    Page<Setmeal> page = new Page(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
    QueryWrapper<Setmeal> setmealQueryWrapper = new QueryWrapper<>();
    if (queryPageBean.getQueryString() != null && !"".equals(queryPageBean.getQueryString())) {
      setmealQueryWrapper.like("code", queryPageBean.getQueryString())
              .or().like("name", queryPageBean.getQueryString())
              .or().like("helpCode", queryPageBean.getQueryString());
    }
    Page<Setmeal> setmealPage = setmealMapper.selectPage(page, setmealQueryWrapper);
    return new PageResult(setmealPage.getTotal(), setmealPage.getRecords());
  }

  @Override
  public Result addSetmeal(Setmeal setmeal, int[] checkgroupIds) {
    int rows = setmealMapper.insert(setmeal);

    if (checkgroupIds != null && checkgroupIds.length > 0) {
      for (int checkgroupId : checkgroupIds) {
        SetmealCheckgroup setmealCheckgroup = new SetmealCheckgroup();
        setmealCheckgroup.setSetmealId(setmeal.getId());
        setmealCheckgroup.setCheckgroupId(checkgroupId);
        setmealCheckgroupMapper.insert(setmealCheckgroup);
      }
    }

    if (rows >= 1) {
      return new Result(true, MessageConstant.ADD_SETMEAL_SUCCESS);
    }
    return new Result(true, MessageConstant.ADD_SETMEAL_FAIL);
  }

  @Override
  public Result getAllSetmeal() {
    List<Setmeal> setmealList = setmealMapper.selectList(null);
    return new Result(true, "", setmealList);
  }

  @Override
  public Result findInfoById(int id) {

    //查询套餐信息
    Setmeal setmeal = setmealMapper.selectById(id);
    //查询套餐所有拥有的检查组的id
    QueryWrapper<SetmealCheckgroup> setmealCheckgroupQueryWrapper = new QueryWrapper<>();
    setmealCheckgroupQueryWrapper.eq("setmeal_id", id);
    List<SetmealCheckgroup> setmealCheckgroupList = setmealCheckgroupMapper.selectList(setmealCheckgroupQueryWrapper);
    //新建一个集合用于存放检查组对象
    List<Checkgroup> checkgroupList = new ArrayList<>();
    //循环套餐与检查组集合
    for (SetmealCheckgroup setmealCheckgroup : setmealCheckgroupList) {
      //查询每个检组
      Checkgroup checkgroup = checkgroupMapper.selectById(setmealCheckgroup.getCheckgroupId());
      //查询该检查组所拥有的检查项的id
      QueryWrapper<CheckgroupCheckitem> checkgroupCheckitemQueryWrapper = new QueryWrapper<>();
      checkgroupCheckitemQueryWrapper.eq("checkgroup_id", checkgroup.getId());
      List<CheckgroupCheckitem> checkgroupCheckitemList = checkgroupCheckitemMapper.selectList(checkgroupCheckitemQueryWrapper);
      //创建一个集合用于存放该检查组所拥的检项集合
      List<Checkitem> checkitemList = new ArrayList<>();
      //循环检查组与检查项集合
      for (CheckgroupCheckitem checkgroupCheckitem : checkgroupCheckitemList) {

        Checkitem checkitem = checkitemMapper.selectById(checkgroupCheckitem.getCheckitemId());
        //把检查项放到集合中
        checkitemList.add(checkitem);
      }
      //把该检查组所拥用的检查项的集合设置给检查组对象
      checkgroup.setCheckItems(checkitemList);
      //把检查组的集合放到checkgroupList中
      checkgroupList.add(checkgroup);

    }
    setmeal.setCheckGroups(checkgroupList);

    return new Result(true, "", setmeal);
  }

  @Override
  public Result getSetmealReport() {

    //定义一个集合用于存放所有的套餐的名称
    List<String> setmealNames = new ArrayList<>();
    //定义一个集合存放套名称及套餐预约数量
    List<Map> setmealCount = new ArrayList<>();
    //统计有多少餐套的名称
    List<Setmeal> setmealList = setmealMapper.selectList(null);
    for (Setmeal setmeal : setmealList) {
      setmealNames.add(setmeal.getName());
      QueryWrapper<Order> orderQueryWrapper = new QueryWrapper<>();
      orderQueryWrapper.eq("setmeal_id", setmeal.getId());
      int value = orderMapper.selectCount(orderQueryWrapper);
      Map map = new HashMap();
      map.put("name", setmeal.getName());
      map.put("value", value);
      setmealCount.add(map);
    }

    Map maps = new HashMap();
    maps.put("setmealNames", setmealNames);
    maps.put("setmealCount", setmealCount);

    return new Result(true, "", maps);
  }
}