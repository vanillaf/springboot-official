package com.offcn.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.offcn.mapper.CheckgroupCheckitemMapper;
import com.offcn.mapper.CheckitemMapper;
import com.offcn.pojo.CheckgroupCheckitem;
import com.offcn.pojo.Checkitem;
import com.offcn.service.CheckItemService;
import com.offcn.util.MessageConstant;
import com.offcn.util.PageResult;
import com.offcn.util.QueryPageBean;
import com.offcn.util.Result;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CheckItemServiceImpl implements CheckItemService {

  @Resource
  private CheckitemMapper checkitemMapper;
  @Resource
  private CheckgroupCheckitemMapper checkgroupCheckitemMapper;

  @Override
  public PageResult getCheckItems(QueryPageBean queryPageBean) {

    //创建分页对象
    Page<Checkitem> page = new Page(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
    //创建查询条件
    QueryWrapper<Checkitem> queryWrapper = new QueryWrapper<>();
    if (queryPageBean.getQueryString() != null && !"".equals(queryPageBean.getQueryString())) {
      queryWrapper.like("code", queryPageBean.getQueryString())
              .or().like("name", queryPageBean.getQueryString());
    }
    //调用selectPage进行查询
    Page<Checkitem> checkitemPage = checkitemMapper.selectPage(page, queryWrapper);
    PageResult pageResult = new PageResult(checkitemPage.getTotal(), checkitemPage.getRecords());
    return pageResult;
  }

  @Override
  public Result addCheckItem(Checkitem checkitem) {

    int rows = checkitemMapper.insert(checkitem);
    if (rows >= 1) {
      return new Result(true, MessageConstant.ADD_CHECKITEM_SUCCESS);
    }
    return new Result(false, MessageConstant.ADD_CHECKITEM_FAIL);
  }

  @Override
  public Result updateCheckItem(Checkitem checkitem) {
    int rows = checkitemMapper.updateById(checkitem);
    if (rows >= 1) {
      return new Result(true, MessageConstant.EDIT_CHECKITEM_SUCCESS);
    }
    return new Result(false, MessageConstant.EDIT_CHECKITEM_FAIL);
  }

  @Override
  public Result deleteCheckItemById(int id) {

    //删除中间表数据
    QueryWrapper<CheckgroupCheckitem> queryWrapper = new QueryWrapper<>();
    queryWrapper.eq("checkitem_id", id);
    checkgroupCheckitemMapper.delete(queryWrapper);
    //再把检查项删除
    int rows = checkitemMapper.deleteById(id);
    if (rows >= 1) {
      return new Result(true, MessageConstant.DELETE_CHECKITEM_SUCCESS);
    }
    return new Result(false, MessageConstant.DELETE_CHECKITEM_FAIL);


  }

  @Override
  public Result getAllCheckitems() {

    List<Checkitem> checkitemList = checkitemMapper.selectList(null);
    return new Result(true, "", checkitemList);
  }
}
