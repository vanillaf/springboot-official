package com.offcn.service;

import com.offcn.util.Result;

import java.util.Map;

public interface OrderService {
    Result submitOrder(Map map);

    Result findOrderById(int id);
}
