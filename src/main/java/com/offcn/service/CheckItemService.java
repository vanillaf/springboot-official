package com.offcn.service;

import com.offcn.pojo.Checkitem;
import com.offcn.util.PageResult;
import com.offcn.util.QueryPageBean;
import com.offcn.util.Result;

public interface CheckItemService {

  PageResult getCheckItems(QueryPageBean queryPageBean);

  Result addCheckItem(Checkitem checkitem);

  Result updateCheckItem(Checkitem checkitem);

  Result deleteCheckItemById(int id);

  Result getAllCheckitems();
}
