package com.offcn.service;

import com.offcn.pojo.Ordersetting;
import com.offcn.util.Result;

import java.util.List;

public interface OrderSettingService {
  void saveBatchOrderSettings(List<String[]> strings);

  Result getOrdersettingByDate(String date);

  Result editNumberByDate(Ordersetting ordersetting);
}
