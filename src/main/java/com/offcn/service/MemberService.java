package com.offcn.service;

import com.offcn.util.Result;

public interface MemberService {
  Result getMemberReport();
}
