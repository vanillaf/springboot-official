package com.offcn.mapper;

import com.offcn.pojo.Setmeal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zs
 * @since 2022-10-11
 */
public interface SetmealMapper extends BaseMapper<Setmeal> {

}
