package com.offcn.mapper;

import com.offcn.pojo.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zs
 * @since 2022-10-11
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
