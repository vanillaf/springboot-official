package com.offcn.mapper;

import com.offcn.pojo.Ordersetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zs
 * @since 2022-10-11
 */
public interface OrdersettingMapper extends BaseMapper<Ordersetting> {

}
