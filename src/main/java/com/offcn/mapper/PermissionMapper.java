package com.offcn.mapper;

import com.offcn.pojo.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zs
 * @since 2022-10-11
 */
public interface PermissionMapper extends BaseMapper<Permission> {

    public List<Permission> getPermissionsByUserId(int id);
}
