package com.offcn.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyConfiguration implements WebMvcConfigurer {

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    //registry.addResourceHandler("/setmealpic/**").addResourceLocations("file:/home/oeyoews/temp/offcnpe-images/upload/");
    registry.addResourceHandler("/setmealpic/**").addResourceLocations("file:/home/oeyoews/workspace/xingyang/upload/");
  }
}
