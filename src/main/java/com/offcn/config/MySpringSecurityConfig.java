package com.offcn.config;

import com.offcn.handler.AuthenticationEntryPointHandler;
import com.offcn.handler.SecurityAccessDeniedHandler;
import com.offcn.handler.SecurityAuthenticationFailureHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Resource;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class MySpringSecurityConfig extends WebSecurityConfigurerAdapter {

  @Resource
  private SecurityAuthenticationFailureHandler authenticationFailureHandler;
  @Resource
  private AuthenticationEntryPointHandler authenticationEntryPoint;
  @Resource
  private SecurityAccessDeniedHandler accessDeniedHandler;
  @Resource
  private UserDetailsService userDetailsService;


  /**
   * 配置密码解析 密码加密器
   *
   * @return
   */
  @Bean
  protected PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  /**
   * 配置用户名和密码进行认证
   *
   * @param auth
   * @throws Exception
   */
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {

    //禁用iframe框架
    http.headers().frameOptions().disable();
    //关闭csrf防护 跨站请求防护
    http.csrf().disable()
            //表单登录
            .formLogin()
            .loginPage("/index.html")
            //提交处理地址
            .loginProcessingUrl("/login")
            //认证成功后，需要跳转到的页面
            .defaultSuccessUrl("/pages/main.html")
            //登陆失败跳转到的页面
            .failureForwardUrl("/index.html")
            //.failureHandler(authenticationFailureHandler)
            .and()
            //认证配置
            .authorizeRequests()
            .antMatchers("/index.html", "/login").permitAll()
            .antMatchers("/css/**", "/img/**", "/js/**", "/loginstyle/**", "/plugins/**").permitAll()
            //任何请求
            .anyRequest()
            //都需要身份验证
            .authenticated();
    //配置无权限访问页面
    //http.exceptionHandling().accessDeniedPage("/uanuth.html");
    // 匿名登陆验证时，执行AuthenticationEntryPointHandler的commence方法
    http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint);
    //没有权限时反回此json格式的数据
    http.exceptionHandling().accessDeniedHandler(accessDeniedHandler);

    //配置退出
    http.logout()
            //退出路径
            .logoutUrl("/logout")
            //退出后跳转页面
            .logoutSuccessUrl("/index.html");


  }
}
